<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Servicemock;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyBehatServicemockBundle extends Bundle
{
}