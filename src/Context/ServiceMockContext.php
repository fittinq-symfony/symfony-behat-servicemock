<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Servicemock\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeStepScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\StepNode;
use Fittinq\Symfony\Behat\Services\Service\ServicesService;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ServiceMockContext implements Context
{
    private ServicesService $servicesService;
    private HttpClientInterface $httpClient;
    private ?StepNode $lastStep = null;

    public function __construct(ServicesService $servicesService, HttpClientInterface $httpClient)
    {
        $this->servicesService = $servicesService;
        $this->httpClient = $httpClient;
    }

    /**
     * @Given /^(.*) service responds$/
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function setServiceResponse(string $service, PyStringNode $string)
    {
        $response = $this->httpClient->request(
            'POST',
            $this->servicesService->getUrl($service) . '/endpoints/responses',
            ["body" => $string->getRaw()]
        );

        $response->getContent();
    }

    /**
     * @Then /^a message should have been sent to (.*)/
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function assertRequest(string $service, PyStringNode $string)
    {
        $response = $this->httpClient->request(
            'POST',
            $this->servicesService->getUrl($service) . '/endpoints/validate',
            ["body" => $string->getRaw()]
        );

        $response->getContent();
    }

    /**
     * @BeforeStep
     */
    public function beforeStep(BeforeStepScope $scope)
    {
        $currentStep = $scope->getStep();

        if ($this->lastStep === null || $this->lastStep->getKeywordType() === 'Given') {
            if ($currentStep->getKeywordType() !== 'Given') {
                $this->httpClient->request('DELETE', "http://service-mock/endpoints");
            }
        }

        $this->lastStep = $currentStep;
    }
}
